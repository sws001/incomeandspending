package com.gitlab.sws.service;

import com.gitlab.sws.domain.Product;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class ProductUtil {

    private ArrayList<Product> productList = new ArrayList<>();

    public void fillProductList() {
        for (int i = 0; i < 5; i++) {
            int price = new Random().nextInt(10000);
            int count = new Random().nextInt(100);
            Product addProduct = new Product(price, "Product" + i, count);
            productList.add(addProduct);
        }
    }

    public List<Product> priceSort() {
        productList.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getPrice().compareTo(o2.getPrice());
            }
        });
        return null;
    }

    public void print() {
        System.out.println(productList);
    }
}
