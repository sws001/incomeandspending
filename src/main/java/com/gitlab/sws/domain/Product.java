package com.gitlab.sws.domain;

public class Product {

    public Product(int price, String name, int count) {
        this.price = price;
        this.name = name;
        this.count = count;
    }

    private Integer price;
    private String name;
    private Integer count;

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Product{");
        sb.append("price=").append(price);
        sb.append(", name='").append(name).append('\'');
        sb.append(", count=").append(count);
        sb.append('}');
        return sb.toString();
    }
}
