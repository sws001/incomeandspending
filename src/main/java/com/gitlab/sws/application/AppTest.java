package com.gitlab.sws.application;

import com.gitlab.sws.service.ProductUtil;

public class AppTest {
    public static void main( String[] args )
    {
        ProductUtil util= new ProductUtil();
        util.fillProductList();
        util.print();
        util.priceSort();
        util.print();
    }
}
